#include <stdio.h>

int main(){
    float b = 0.1;
    int a = sizeof(b);
    printf("float: %d\n", a);
    long long ago; // in a galaxy far, far away.
    int sizeof_ago = sizeof(ago);
    printf("long: %d\n", sizeof_ago);
    return 0;
}
