#include <stdio.h>

void local_printf(char c, int * length) {
    int i;
    for ( i = 0; i < *length; i++ ) {
        // FD1 is stdout
        //write(1, c[i], 1);
        syscall(4, 1, c[i], 1);
    }
    printf("%c", c);
}

void flush_buffer() {
    syscall(4, 1, "\r\n", 2);
}

int main() {
    char a[] = "Hello\n";
    int len = 5;
    int length = &len;
    local_printf(a,*length);
   }
}
