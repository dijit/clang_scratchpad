#include <unistd.h>

int main(){
    pledge("stdout", NULL); // Still required even if
    char a[6] = "Hello\n";  // you're just writing to fd.
    write(0, a, sizeof(a));
    _exit(0);
}
