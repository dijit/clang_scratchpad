#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/inotify.h>

#define EVENT_SIZE ( sizeof (struct inotify_event) )
#define BUF_LEN ( 1024 * ( EVENT_SIZE + 16) )
#define DIRECTORY "/tmp"

int main( int argc, char **argv )
{
    int length, i = 0;
    int fd;
    int wd;
    char buffer[BUF_LEN];

    fd = inotify_init();

    if ( fd < 0 ) {
        perror( "inotify didn't init\n");
    }

    wd = inotify_add_watch( fd, DIRECTORY,
            IN_MODIFY | IN_CREATE | IN_DELETE );
    length = read( fd, buffer, BUF_LEN );

    if ( length < 0 ) {
        perror("Error while reading\n");
    }
    
    while (i < length ) {
        struct inotify_event *event = ( struct inotify_event * ) &buffer[i];
        if ( event->len ) {
            if ( event->mask & IN_CREATE ) {
                if ( event->mask & IN_ISDIR ) {
                    printf("The directory %s was created.\n", event->name);
                }
                else { 
                    printf("The file %s was created.\n", event->name);
                }
            }
            else if ( event->mask & IN_DELETE ) {
                if ( event->mask & IN_ISDIR ) {
                    printf("The directory %s was modified", event->name);
                }
                else {
                    printf("The file %s was modified", event->name);
                }
            }
        }
        i += EVENT_SIZE + event->len;
    }
    (void) inotify_rm_watch(fd, wd);
    (void) close (fd);
    exit(0);
}
