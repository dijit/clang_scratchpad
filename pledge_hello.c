#include <stdio.h>
#include <unistd.h>

#ifndef __OpenBSD__
char
pledge(char * a, int * b)
{};
#endif

int main() {
    //pledge("stdio", NULL);
    //printf("Hello world!\n");
    pledge("exec proc", NULL);
    execl("/bin/echo", "echo", "Hello world!", NULL);
    _exit(0);
}
