#include <unistd.h>
#include <cstddef> 
#include <assert.h>

namespace 
{
    class LinkedList 
    {
        public:
            LinkedList()
            {
                myHead      = NULL; 
                myCount     = 0; 
            }

            ~LinkedList()
            {
                // delete all allocated memory 
                while (myHead != NULL)
                {
                    Node* prevNode = myHead; 
                    myHead = myHead->myNext; 
                    delete prevNode; 
                }
            }

            int 
            Count()
            {
                return myCount; 
            }

            void 
            Add(
                    int    aAge, 
                    int    aZipCode)
            {
                Node* t = new Node(aAge, aZipCode);
                if(myHead == NULL)
                {
                    myHead      = t; 
                }
                else 
                {
                    t->myNext   = myHead;
                    myHead      = t;
                }
                myCount++;
            }

            void 
            Remove(
                    int    aAge, 
                    int    aZipCode)
            {
                // Iterate over list:
                Node* myCursor = myHead;
                Node* myPrevious = NULL;
                while(myCursor != NULL)
                {
                    if( myCursor->myAge == aAge && 
                        myCursor->myZipCode == aZipCode)
                    {
                        if(myPrevious == NULL)
                        {
                            myHead = myCursor->myNext;
                            delete myCursor;
                            myCursor = myHead;
                        }
                        else
                        {
                            myPrevious->myNext = myCursor->myNext;
                            delete myCursor;
                            myCursor = myPrevious->myNext;
                        }
                        myCount--;
                    }
                    else
                    {
                        // skip
                        myPrevious = myCursor;
                        myCursor = myCursor->myNext;
                    }
                }
            }

            bool
            Exists(
                   int     aAge, 
                   int     aZipCode)
            {
                // return true if we find a matching element
                Node* myCursor = myHead;
                while(myCursor != NULL)
                {
                    if( myCursor->myAge == aAge &&
                        myCursor->myZipCode == aZipCode)
                    {
                        return true;
                    }
                    myCursor = myCursor->myNext;
                }
                return false;
            }

            class Node 
            {
                public:
                    Node()
                    {

                    }
                    Node(
                         int             aAge, 
                         int             aZipCode)
                    {
                        myAge            = aAge; 
                        myZipCode        = aZipCode; 
                        myNext           = NULL;
                    }

                    ~Node()
                    {
                    }
                    int              myAge; 
                    int              myZipCode; 
                    Node*            myNext; 
            };
            Node*            myHead; 
            int              myCount; 
    };
}

int
main()
{
    pledge("stdio proc", NULL);
    LinkedList a; 
    a.Add(7, 98765); 
    a.Add(21, 12345); 
    a.Add(55, 21211);
    assert(a.Count() == 3);
    assert(a.Exists(21, 12345));
    a.Remove(100, 9999999); // Does not exist
    a.Remove(7, 98765);
    a.Remove(55, 21211);
    assert(a.Count() == 1);
    return(0);
}
